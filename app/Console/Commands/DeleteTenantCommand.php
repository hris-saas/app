<?php

namespace App\Console\Commands;

use App\Jobs\CreateTenant;
use HRis\Core\Eloquent\Tenant;
use HRis\Core\Generators\UuidGenerator;
use Illuminate\Console\Command;

class DeleteTenantCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenancy:delete-tenant {tenant}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete a tenant website.';

    /**
     * Execute the console command.
     */
    public function __invoke(): void
    {
        $tenant = $this->argument('tenant');

        if ($tenant == 'all') {
            $this->deleteAllTenants();

            return;
        }

        $domain = $tenant . '.' . config('app.domain');

        $data = [
            'uuid'                  => (new UuidGenerator())->generate(),
            'fqdn'                  => $domain,
            'first_name'            => ucfirst($tenant),
            'last_name'             => 'Admin',
            'email'                 => "{$tenant}@hris-saas.com",
            'password'              => bcrypt('password'),
            'password_confirmation' => bcrypt('password'),
            'name'                  => ucfirst($tenant),
        ];

        $website = (new CreateTenant($data))->handle();

        $this->info('New Tenant Created: '. $domain . PHP_EOL);
    }

    private function deleteAllTenants()
    {
        $tenants = Tenant::all();

        foreach ($tenants as $tenant) {
            $tenant->delete();
        }
    }
}
